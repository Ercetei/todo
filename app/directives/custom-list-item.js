'use strict';

angular.module('myApp.view1')
    .filter('startFrom', function() {
        return function(input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
    })
    .directive('customListItem', function(focus)
    {
        return  {
            restrict: 'EA',
            scope: {
                data_list: '=d'
            },
            templateUrl: 'templates/list-item.html',
            link: function(scope, element, attr){
                scope.checked = false;
                scope.checkedAll = false;
                scope.currentPage = 0;
                scope.pageSize = 3;
                scope.numberOfPages=function(){
                    return Math.ceil(scope.data_list.length/scope.pageSize);
                };
                scope.edit = function (task) {
                    scope.editTask = task;
                    focus('test');
                    // Clone the original task to restore it on demand.
                    scope.originalTask = angular.extend({}, task);
                };

                scope.closeEdit = function()
                {
                    scope.editTask = null;
                };

                scope.testCheck = function(d)
                {
                    if(scope.checkedAll && d.done == false)
                    {
                        scope.checkedAll = false;
                        scope.checked = false;
                    }

                    var allchecked = true;
                    scope.data_list.forEach(function(d){
                        if(d.done == false){
                            allchecked = false;
                        }
                    });

                    if(allchecked)
                    {
                        scope.checkedAll = true;
                        scope.checked = true;
                    }
                };

                scope.toggleCheck = function()
                {
                    console.log(scope.checked);

                    if(scope.checked){
                        scope.data_list.forEach(function(d){
                            d.done = true;
                            scope.checkedAll = false;
                        })
                    }else{
                        scope.data_list.forEach(function(d){
                            d.done = false;
                            scope.checkedAll = true;
                        })
                    }
                };

                scope.removeData = function(d) {
                    scope.data_list.splice(scope.data_list.indexOf(d), 1);
                };
            }
        }
    });