angular.module('myApp.view1')
    .controller('View1Ctrl', ['$scope' , '$http', 'todoList',  function($scope, $http, todoList) {
        $scope.data = todoList.data;
        $scope.editTask = null;
        
        $scope.addData = function()
        {
            if($scope.newData){
                $scope.data.push($scope.newData);
                delete $scope.newData;
            }
        };

        $scope.removeFinished = function()
        {
            $scope.data = $scope.data.filter( function( item ){
                return !item.done
            });
        };
}]);