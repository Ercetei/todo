'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
        'myApp.view1',
        'myApp.view2',
        'myApp.version',
        'ui.router'
    ])

    .config(['$locationProvider', '$stateProvider', function ($locationProvider, $stateProvider) {
        $locationProvider.hashPrefix('!');
        $stateProvider
            .state("otherwise", {url: '/view1', templateUrl: 'view1/view1.html'})
    }])

    .run(function ($transitions, $rootScope) {
        $transitions.onStart({},
            function () {
                $rootScope.spinner = true
            });

        $transitions.onSuccess({},
            function () {
                $rootScope.spinner = false
            });
    });