'use strict';

angular.module('myApp.view1', ['ngMaterial', 'ui.router'])
    .config(['$stateProvider' , function($stateProvider) {
        $stateProvider.state('view1',{
            url:"/view1",
            templateUrl:"view1/view1.html",
            controller:'View1Ctrl',
            resolve:{
                todoList: function($http, $timeout)
                {
                    return $http.get("data/todo.json").then(function(data){
                        return $timeout(function(){
                            return data;
                        }, 2000);
                    });
                }
            }
        })

    }]);

